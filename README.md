# Server Contents
This repository comprises *strictly public* aspects of the jsonvillanueva.com
server. Server contents that are private can be asked for at
a@jsonvillanueva.com. Should failure occur, the server can easily be
re-initiated. This repo currently contains:
- Apache server content from the root of the domain hierarchy


